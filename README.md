# blackjack

A simple blackjack CLI game.

## Build

You need to have build2 installed

```bash
# Create a build config
bdep init -C ../blackjack-gcc @gcc cc config.cxx=g++

# Build the project
b
```
