#ifndef BLACKJACK_DECK
#define BLACKJACK_DECK

#include <vector>
#include <blackjack/card.hxx>

namespace blackjack
{
    class deck
    {
    public:
        deck();
        card draw_card();
        std::size_t size() const;

    private:
        std::vector<card> _cards;
    };
}

#endif
