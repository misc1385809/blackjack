#ifndef BLACKJACK_HAND_STATUS
#define BLACKJACK_HAND_STATUS

#include <ostream>

namespace blackjack
{
    enum class hand_status
    {
        pending,
        stand,
        bust
    };

    std::ostream& operator<<(std::ostream& ostream, hand_status hand_status);
}

#endif
