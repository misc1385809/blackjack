#ifndef BLACKJACK_SUIT
#define BLACKJACK_SUIT

#include <ostream>

namespace blackjack
{
    enum class suit
    {
        clubs,
        diamonds,
        hearts,
        spades
    };

    std::ostream& operator<<(std::ostream& ostream, suit suit);
}

#endif
