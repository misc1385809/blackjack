#ifndef BLACKJACK_CARD
#define BLACKJACK_CARD

#include <blackjack/rank.hxx>
#include <blackjack/suit.hxx>

namespace blackjack
{
    class card
    {
    public:
        card(rank rank, suit suit);
        rank get_rank() const;
        suit get_suit() const;

    private:
        rank _rank;
        suit _suit;
    };
}

#endif
