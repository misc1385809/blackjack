#include <blackjack/hand.hxx>

namespace blackjack
{
    namespace
    {
        int get_card_value(const card& card)
        {
            switch (card.get_rank())
            {
            case rank::ace:
                return 11;

            case rank::two:
                return 2;

            case rank::three:
                return 3;

            case rank::four:
                return 4;

            case rank::five:
                return 5;

            case rank::six:
                return 6;

            case rank::seven:
                return 7;
            
            case rank::eight:
                return 8;

            case rank::nine:
                return 9;

            case rank::ten:
            case rank::jack:
            case rank::queen:
            case rank::king:
                return 10;

            default:
                return 0;
            }
        }
    }

    hand::hand() : _hand_status(hand_status::pending)
    {
    }

    void hand::hit(const card &card)
    {
        _cards.push_back(card);

        if (get_value() > 21)
        {
            _hand_status = hand_status::bust;
        }
    }

    void hand::stand()
    {
        _hand_status = hand_status::stand;
    }

    int hand::get_value() const
    {
        int value = 0;
        int aces = 0;

        for (const auto& card : _cards)
        {
            value += get_card_value(card);
            if (card.get_rank() == rank::ace)
            {
                ++aces;
            }
        }

        while (value > 21 && aces > 0)
        {
            value -= 10;
            --aces;
        }

        return value;
    }

    hand_status hand::get_hand_status() const
    {
        return _hand_status;
    }

    const std::vector<card>& hand::get_cards() const
    {
        return _cards;
    }
}
