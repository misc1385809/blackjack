#include <blackjack/rank.hxx>

namespace blackjack
{
    std::ostream& operator<<(std::ostream& ostream, rank rank)
    {
        switch (rank)
        {
        case rank::ace:
            ostream << "ace";
            break;

        case rank::two:
            ostream << "two";
            break;

        case rank::three:
            ostream << "three";
            break;

        case rank::four:
            ostream << "four";
            break;

        case rank::five:
            ostream << "five";
            break;

        case rank::six:
            ostream << "six";
            break;

        case rank::seven:
            ostream << "seven";
            break;

        case rank::eight:
            ostream << "eight";
            break;

        case rank::nine:
            ostream << "nine";
            break;

        case rank::ten:
            ostream << "ten";
            break;

        case rank::jack:
            ostream << "jack";
            break;

        case rank::queen:
            ostream << "queen";
            break;

        case rank::king:
            ostream << "king";
            break;

        default:
            ostream << static_cast<int>(rank);
            break;
        }

        return ostream;
    }
}
