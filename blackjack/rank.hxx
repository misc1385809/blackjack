#ifndef BLACKJACK_RANK
#define BLACKJACK_RANK

#include <ostream>

namespace blackjack
{
    enum class rank
    {
        ace,
        two,
        three,
        four,
        five,
        six,
        seven,
        eight,
        nine,
        ten,
        jack,
        queen,
        king
    };

    std::ostream& operator<<(std::ostream& ostream, rank rank);
}

#endif