#include <blackjack/deck.hxx>

#include <algorithm>
#include <array>
#include <random>
#include <blackjack/rank.hxx>
#include <blackjack/suit.hxx>

namespace blackjack
{
    static std::array _ranks
    {
        rank::ace,
        rank::two,
        rank::three,
        rank::four,
        rank::five,
        rank::six,
        rank::seven,
        rank::eight,
        rank::nine,
        rank::ten,
        rank::jack,
        rank::queen,
        rank::king
    };

    static std::array _suits
    {
        suit::clubs,
        suit::diamonds,
        suit::hearts,
        suit::spades
    };

    deck::deck()
    {
        for (auto suit : _suits)
        {
            for (auto rank : _ranks)
            {
                _cards.push_back(card(rank, suit));
            }
        }

        std::random_device device;
        std::default_random_engine engine(device());
        std::shuffle(std::begin(_cards), std::end(_cards), engine);
    }

    card deck::draw_card()
    {
        if (size() == 0)
        {
            throw std::range_error("deck empty");
        }

        auto front(_cards.begin());
        auto card(*front);
        _cards.erase(front);
        return card;
    }

    std::size_t deck::size() const
    {
        return _cards.size();
    }
}
