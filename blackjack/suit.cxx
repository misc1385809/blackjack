#include <blackjack/suit.hxx>

namespace blackjack
{
    std::ostream& operator<<(std::ostream& ostream, suit suit)
    {
        switch (suit)
        {
        case suit::clubs:
            ostream << "clubs";
            break;

        case suit::diamonds:
            ostream << "diamonds";
            break;

        case suit::hearts:
            ostream << "hearts";
            break;

        case suit::spades:
            ostream << "spades";
            break;

        default:
            ostream << static_cast<int>(suit);
            break;
        }

        return ostream;
    }
}
