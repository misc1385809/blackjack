#include <blackjack/player.hxx>

#include <cmath>

namespace blackjack
{
    player::player(long credits) : _credits(credits)
    {
        _hands.emplace_back();
    }

    long player::get_credits() const
    {
        return _credits;
    }

    long player::bet(long credits)
    {
        long bet = std::min(credits, _credits);
        _credits -= bet;
        return bet;
    }

    void player::win(long credits)
    {
        _credits += credits;
    }

    std::vector<hand>& player::get_hands()
    {
        return _hands;
    }
}
