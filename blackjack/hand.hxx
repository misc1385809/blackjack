#ifndef BLACKJACK_HAND
#define BLACKJACK_HAND

#include <vector>
#include <blackjack/card.hxx>
#include <blackjack/hand_status.hxx>

namespace blackjack
{
    class hand
    {
    public:
        hand();

        void hit(const card &card);

        void stand();

        int get_value() const;

        hand_status get_hand_status() const;

        const std::vector<card>& get_cards() const;

    private:
        hand_status _hand_status;
        std::vector<card> _cards;
    };
}

#endif
