#include <iostream>
#include <blackjack/deck.hxx>
#include <blackjack/hand_status.hxx>
#include <blackjack/player.hxx>

int main(int argc, char** argv)
{
    blackjack::deck deck;
    blackjack::player dealer(1000);
    blackjack::player player(1000);

    auto &player_hand = player.get_hands()[0];
    auto &dealer_hand = dealer.get_hands()[0];

    player_hand.hit(deck.draw_card());
    dealer_hand.hit(deck.draw_card());
    player_hand.hit(deck.draw_card());

    while (player_hand.get_hand_status() == blackjack::hand_status::pending)
    {
        std::cout << "dealers cards:" << std::endl;
        for (auto& card : dealer_hand.get_cards())
        {
            std::cout << card.get_rank() << " of " << card.get_suit() << std::endl;
        }

        std::cout << std::endl;

        std::cout << "players cards:" << std::endl;
        for (auto& card : player_hand.get_cards())
        {
            std::cout << card.get_rank() << " of " << card.get_suit() << std::endl;
        }

        std::cout << std::endl;

        std::cout << "[1] hit" << std::endl;
        std::cout << "[2] stand" << std::endl;
        std::cout << "choice: " << std::endl;

        int choice;
        std::cin >> choice;

        if (choice == 1)
        {
            player_hand.hit(deck.draw_card());
        }
        else if (choice == 2)
        {
            player_hand.stand();
        }

        std::cout << std::endl;
    }

    if (player_hand.get_hand_status() == blackjack::hand_status::bust)
    {
        std::cout << "you lose" << std::endl;
        return 0;
    }

    while (dealer_hand.get_hand_status() == blackjack::hand_status::pending)
    {

        if (dealer_hand.get_value() < 17)
        {
            dealer_hand.hit(deck.draw_card());
        }
        else
        {
            dealer_hand.stand();
        }
    }

    if (dealer_hand.get_hand_status() == blackjack::hand_status::bust)
    {
        std::cout << "you win" << std::endl;
        return 0;
    }

    std::cout << "dealers cards:" << std::endl;
    for (auto& card : dealer_hand.get_cards())
    {
        std::cout << card.get_rank() << " of " << card.get_suit() << std::endl;
    }

    std::cout << std::endl;

    std::cout << "players cards:" << std::endl;
    for (auto& card : player_hand.get_cards())
    {
        std::cout << card.get_rank() << " of " << card.get_suit() << std::endl;
    }

    std::cout << std::endl;

    int dealer_value = dealer_hand.get_value();
    int player_value = player_hand.get_value();

    std::cout << "dealer: " << dealer_value << std::endl;
    std::cout << "player: " << player_value << std::endl;

    if (dealer_value < player_value)
    {
        std::cout << "you win" << std::endl;
    }
    else if (player_value < dealer_value)
    {
        std::cout << "you lose" << std::endl;
    }
    else
    {
        std::cout << "draw" << std::endl;
    }

    return 0;
}
