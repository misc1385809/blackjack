#include <blackjack/hand_status.hxx>

namespace blackjack
{
    std::ostream& operator<<(std::ostream& ostream, hand_status hand_status)
    {
        switch (hand_status)
        {
        case hand_status::bust:
            ostream << "bust";
            break;

        case hand_status::pending:
            ostream << "pending";
            break;
            
        case hand_status::stand:
            ostream << "stand";
            break;

        default:
            ostream << static_cast<int>(hand_status);
            break;
        }

        return ostream;
    }
}
