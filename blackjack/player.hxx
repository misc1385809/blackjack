#ifndef BLACKJACK_PLAYER
#define BLACKJACK_PLAYER

#include <vector>
#include <blackjack/hand.hxx>

namespace blackjack
{
    class player
    {
    public:
        player(long credits);

        long get_credits() const;

        long bet(long credits);

        void win(long credits);

        std::vector<hand>& get_hands();

    private:
        long _credits;
        std::vector<hand> _hands;
    };
}

#endif
