#include <blackjack/card.hxx>

namespace blackjack
{
    card::card(rank rank, suit suit) : _rank(rank), _suit(suit)
    {
    }

    rank card::get_rank() const
    {
        return _rank;
    }

    suit card::get_suit() const
    {
        return _suit;
    }
}
